---
title: Tech Resources
subtitle: Save money on student infra.
comments: false
---

Turns out someone already did the work --- just use this : [https://free-for.dev/](https://free-for.dev/).

### GCP Student Discount

[https://edu.google.com/intl/en_ca/programs/students](https://edu.google.com/intl/en_ca/programs/students)

Not sure if you still need a credit card. After you use it up though I'm not sure if you will want to stick around --- GCP is relatively more expensive.
But it allows you about 300 dollars of free virtual services... which can last you a year if you don't use the more
expensive boxes.

### Github Student Pack

[https://education.github.com/pack](https://education.github.com/pack)

Bunch of free services while you still have your university email. It's free. Take it.

### Static Site Hosts

Github, Gitlab, and Netlify are all services which allow you to host static sites (read: no database) like this one --- and it deploys every time
you make a commit. Combo it with one of those free domain names and you have yourself a free website to advertise yourself...
at least for a year before you need to either renew or use the default static site domain.