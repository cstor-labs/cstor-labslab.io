**This wiki is in early stages of development!**

This is a general wiki resource intended mainly for UTSG CS students, 
but most resources should be useful for CS students in Toronto in general.

### Contributing

The source may be found at the [project page](https://gitlab.com/cstor-labs/wiki).

Please reach out to `beyond L#9756` on Discord if you wish to be added as a contributor, or otherwise 
- have tips or suggestions for improvement.
- would like to report inaccurate, misleading, or downright bad advice.
    - would like to report any form of advertising, especially that of which is not necessarily useful for CS students.
